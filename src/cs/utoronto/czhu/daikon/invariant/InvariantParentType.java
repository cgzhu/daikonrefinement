package cs.utoronto.czhu.daikon.invariant;

public class InvariantParentType {
	//parent type
	public static final int CLASS = 100;
	public static final int METHOD = 101;
	public static final int OBJECT = 102;
	
	public static boolean isUninterestingParent(String entityString)
	{
		if(entityString.startsWith("org.junit.")) return true;
		if(entityString.startsWith("com.sun.")) return true;
		if(entityString.startsWith("junit.")) return true;
		
		return false;
	}
	
	public static String parentTypeToString(int parentType)
	{
		switch (parentType)
		{
			case InvariantParentType.CLASS:
				return "CLASS";
			case InvariantParentType.METHOD:
				return "METHOOD";
			case InvariantParentType.OBJECT:
				return "OBJECT";
			default:
				return "OTHER";
		}
	}
	
	public static int getParentTypeFromFirstInvariant(String firstInvariant)
	{
		if(firstInvariant.endsWith("CLASS"))
		{
			return InvariantParentType.CLASS;
		}
		else if(firstInvariant.endsWith("OBJECT"))
		{
			return InvariantParentType.OBJECT;
		}
		else
		{
			return InvariantParentType.METHOD;
		}
	}
}
