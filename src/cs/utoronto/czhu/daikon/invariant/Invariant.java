package cs.utoronto.czhu.daikon.invariant;

public class Invariant {
	private String fullName;
	private String parentName;
	private String parentFullName;

	private int type;
	private int parentType;
	
	public String getParentFullName() {
		return parentFullName;
	}
	public void setParentFullName(String parentFullName) {
		this.parentFullName = parentFullName;
	}
	public int getParentType() {
		return parentType;
	}
	public void setParentType(int parentType) {
		this.parentType = parentType;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj.getClass() != Invariant.class){return false;}
		return this.fullName.equals(((Invariant)obj).getFullName()) 
				&& this.parentFullName.equals(((Invariant)obj).getParentFullName());
	}
	
	@Override
	public int hashCode() {
		return 100;
	}
}
