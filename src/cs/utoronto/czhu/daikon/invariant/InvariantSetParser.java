package cs.utoronto.czhu.daikon.invariant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import cs.utoronto.czhu.daikon.utils.PrintUtils;

public class InvariantSetParser {
	
	public Set<Invariant> loadFlatInvariantSetFromHierarchicalSet(Set<Set<Invariant>> hSet)
	{
		Set<Invariant> fSet = new LinkedHashSet<>();
		for(Set<Invariant> set : hSet)
		{
			for(Invariant i : set)
			{
				fSet.add(i);
			}
		}
		return fSet;
	}
	
	public Set<Set<Invariant>> parseInvariantSetFromDaikonOutputFile(String filePath)
	{
		Set<Set<Invariant>> fileInvariantSet = new LinkedHashSet<>();
		File f = new File(filePath);
		FileInputStream in;
		try 
		{
			in = new FileInputStream(f);
			byte[] bytes = new byte[in.available()];
			in.read(bytes);
			String textContent = new String(bytes);
			String[] splitedContent = textContent.split("===========================================================================\n");
			int methodNumber = 0;
			for(int i=1; i< splitedContent.length; i++)
			{
				if(InvariantParentType.isUninterestingParent(splitedContent[i]))
				{
					continue;
				}
				Set<Invariant> methodInvariantSet = new LinkedHashSet<>();
				String[] splitedMethodContent = splitedContent[i].split("\n");
				PrintUtils.print("[INVARIANT NUMBER]: " + splitedMethodContent.length);
				for(String invariantString : splitedMethodContent)
				{
					if(invariantString.startsWith("warning:"))
					{
						continue;
					}
					Invariant inv = new Invariant();
					inv.setFullName(invariantString);
					inv.setType(0);
					inv.setParentFullName(splitedContent[i].split("\n")[0]);
					inv.setParentName(splitedContent[i].split(":::")[0]);
					inv.setParentType(InvariantParentType.getParentTypeFromFirstInvariant(splitedContent[i]));
					
					methodInvariantSet.add(inv);
				}
				fileInvariantSet.add(methodInvariantSet);
				methodNumber += 1;
			}
			PrintUtils.print("[METHOD NUMBER]: " + methodNumber);
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fileInvariantSet;
	}
	
	public Map<String, Set<Set<String>>> parseInvariantSetFromDaikonOutputDir(String dirPath)
	{
		Map<String, Set<Set<String>>> allInvariantMap = new TreeMap<>();
		File dir = new File(dirPath);
		File[] fileList = dir.listFiles();
		PrintUtils.print("[FILE NUMBER]: " + fileList.length);
		for(File f : fileList)
		{
			if(f.isFile())
			{
				try 
				{
					Set<Set<String>> fileInvariantSet = new LinkedHashSet<>();
					FileInputStream in = new FileInputStream(f);
					byte[] bytes = new byte[in.available()];
					in.read(bytes);
					String textContent = bytes.toString();
					String[] splitedContent = textContent.split("===========================================================================\n");
					PrintUtils.print("[METHOD NUMBER]: " + (splitedContent.length-1));
					for(int i=1; i< splitedContent.length; i++)
					{
						Set<String> methodInvariantSet = new LinkedHashSet<>();
						String[] splitedMethodContent = splitedContent[i].split("\n");
						PrintUtils.print("[INVARIANT NUMBER]: " + splitedMethodContent.length);
						for(String invariantString : splitedMethodContent)
						{
							methodInvariantSet.add(invariantString);
						}
						fileInvariantSet.add(methodInvariantSet);
					}
					allInvariantMap.put(f.getName(), fileInvariantSet);
					in.close();
				} 
				catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return allInvariantMap;
	}
	
	
}
