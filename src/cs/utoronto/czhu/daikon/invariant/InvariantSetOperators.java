package cs.utoronto.czhu.daikon.invariant;

import java.util.LinkedHashSet;
import java.util.Set;

public class InvariantSetOperators {
	
	public static Set<Invariant> SetAMinusSetB(Set<Invariant> A, Set<Invariant> B)
	{
		Set<Invariant> AminusB = new LinkedHashSet<>();
		for(Invariant i : A)
		{
			if(!B.contains(i))
			{
				AminusB.add(i);
			}
		}
		return AminusB;
	}
}
