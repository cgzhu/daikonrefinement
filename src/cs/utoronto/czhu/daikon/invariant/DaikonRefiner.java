package cs.utoronto.czhu.daikon.invariant;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import cs.utoronto.czhu.daikon.utils.PrintUtils;

public class DaikonRefiner {
	public static void main(String[] args)
	{
		InvariantSetParser parser = new InvariantSetParser();
		
		Set<Set<Invariant>> hInvariantSet = parser.parseInvariantSetFromDaikonOutputFile("/home/polaris/Desktop/123/v0");
		Set<Invariant> fInvariantSet = parser.loadFlatInvariantSetFromHierarchicalSet(hInvariantSet);
		
		Set<Set<Invariant>> hInvariantSet2 = parser.parseInvariantSetFromDaikonOutputFile("/home/polaris/Desktop/123/v1");
		Set<Invariant> fInvariantSet2 = parser.loadFlatInvariantSetFromHierarchicalSet(hInvariantSet2);
		
		Set<Invariant> AminusB = InvariantSetOperators.SetAMinusSetB(fInvariantSet, fInvariantSet2);
		
		for(Invariant i : fInvariantSet)
		{
			PrintUtils.print("----------------------------------------------------");
			PrintUtils.print("[FULL NAME]: " + i.getFullName());
			PrintUtils.print("[PARENT FULL NAME]: " + i.getParentFullName());
			PrintUtils.print("[PARENT TYPE]: " + InvariantParentType.parentTypeToString(i.getParentType()));
		}
		
		try 
		{
			FileWriter writer = new FileWriter(new File("/home/polaris/Desktop/123/v0-v1"));
			
			for(Invariant i : AminusB)
			{
				writer.write("----------------------------------------------------\n");
				writer.write("[FULL NAME]: " + i.getFullName() + "\n");
				writer.write("[PARENT FULL NAME]: " + i.getParentFullName() + "\n");
				writer.write("[PARENT TYPE]: " + InvariantParentType.parentTypeToString(i.getParentType()) + "\n");
			}
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
